package com.example.dawidzylak.simplerssreader;

import android.app.ListActivity;

import android.os.StrictMode;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleRssReaderActivity extends ListActivity {
	List title;
	List desc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_rss_reader);
		title = new ArrayList();
		desc = new ArrayList();
		Button mSearchBtn = (Button) findViewById(R.id.searchBtn);
		final EditText mUrl = (EditText) findViewById(R.id.urlET);
		mSearchBtn.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				mUrl.getText();

			}
		});


		int SDK_INT = android.os.Build.VERSION.SDK_INT;
		if (SDK_INT > 8) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
			//your codes here

		}
		getDataFromXML();
	}

	public void getDataFromXML() {

		try {
			URL url = new URL("http://allegro.pl/rss.php/search?string=iphone+6&selected_country=1&search_type=1&postcode_enabled=1");

			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(false);
			XmlPullParser xpp = factory.newPullParser();

			// We will get the XML from an input stream

			xpp.setInput(getInputStream(url), "UTF_8");

        /* We will parse the XML content looking for the "<title>" tag which appears inside the "<item>" tag.
         * However, we should take in consideration that the rss feed name also is enclosed in a "<title>" tag.
         * As we know, every feed begins with these lines: "<channel><title>Feed_Name</title>...."
         * so we should skip the "<title>" tag which is a child of "<channel>" tag,
         * and take in consideration only "<title>" tag which is a child of "<item>"
         *
         * In order to achieve this, we will make use of a boolean variable.
         */
			boolean insideItem = false;

			// Returns the type of current event: START_TAG, END_TAG, etc..
			int eventType = xpp.getEventType();
			String x;
			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_TAG) {

					if (xpp.getName().equalsIgnoreCase("item")) {
						insideItem = true;
					} else if (xpp.getName().equalsIgnoreCase("title")) {
						if (insideItem)
							title.add(xpp.nextText()); //title
					} else if (xpp.getName().equalsIgnoreCase("description")) {
						if (insideItem) {
							//x = xpp.nextText();
							x = ">sebaspg</a><br /> Cena Kup Teraz: 2 939,00 zł<br /> Do końca: 2 dni (śro, 11 maj 2016,";
							x = getPriceFromXmlDescription(x);
							//getPriceFromXmlDescription(x);
							desc.add(x);
							//x = "Cena Kup Teraz: 2 949,00 zł<br /> Do końca: 2 dni tyle ni";
							//desc.add(getPriceFromXmlDescription(x));
						}

							//desc.add(xpp.nextText()); //description
					}
				} else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
					insideItem = false;
				}

				eventType = xpp.next(); //move to next element
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_list_item_1, desc);

		setListAdapter(adapter);
		// Binding data
}
	public String getPriceFromXmlDescription(String description){

		Pattern p = Pattern.compile("/\\d[\\d\\s]*,\\d{2}\\s/", Pattern.UNICODE_CASE);
		Matcher m = p.matcher(description);
		if(m.find())
		 return m.group();
		else
		return  m.group();
	}


	public InputStream getInputStream(URL url) {
		try {
			return url.openConnection().getInputStream();
		} catch (IOException e) {
			return null;
		}
	}

}
